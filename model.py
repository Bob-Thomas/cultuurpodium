__author__ = 'bob'

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import security

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:noob007@localhost/cultuurpodium'
db = SQLAlchemy(app)


class Persbericht(db.Model):
    persbericht_id = db.Column(db.Integer, primary_key=True)
    titel = db.Column(db.String(80), nullable=False)
    content = db.Column(db.Text, nullable=False)

    def __init__(self, titel=None, content=None):
        self.titel = titel
        self.content = content

    def __repr__(self):
        return '<Persbericht %s %s>' % (self.persbericht_id, self.titel)


class Zaal(db.Model):
    zaal_id = db.Column(db.Integer, primary_key=True)
    naam = db.Column(db.String(80), nullable=False)
    status = db.Column(db.Enum('vrij', 'schoonmaak', 'ingebruik'), nullable=False)
    plaatsen = db.Column(db.Integer, nullable=False)

    def __init__(self, naam=None, status=None, plaatsen=None):
        self.naam = naam
        self.status = status
        self.plaatsen = plaatsen

    def __repr__(self):
        return '<Zaal %s %s>' % (self.zaal_id, self.naam)


class Klant(db.Model):
    klant_id = db.Column(db.Integer, primary_key=True)
    voornaam = db.Column(db.String(80), nullable=False)
    achternaam = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(80), primary_key=True, nullable=False)
    wachtwoord = db.Column(db.Text, nullable=False)
    bestellingen = db.relationship('Bestelling', lazy='dynamic')

    def __init__(self, voornaam=None, achternaam=None, email=None, wachtwoord=None):
        self.voornaam = voornaam
        self.achternaam = achternaam
        self.email = email
        self.wachtwoord = security.generate_password_hash(wachtwoord, method='pbkdf2:sha256:2000', salt_length=8)

    def check_password(self, wachtwoord):
        return security.check_password_hash(self.wachtwoord, wachtwoord)

    def __repr__(self):
        return '<Klant %s %s>' % (self.klant_id, self.naam)


class Genre(db.Model):
    genre_id = db.Column(db.Integer, primary_key=True)
    naam = db.Column(db.String(80), primary_key=True)

    def __init__(self, naam=None):
        self.naam = naam

    def __repr__(self):
        return '<Genre %s %s>' % (self.genre_id, self.naam)


class MailingList(db.Model):
    mailinglist_id = db.Column(db.Integer, primary_key=True)
    klant = db.Column(db.Integer, db.ForeignKey('klant.klant_id',
                                                onupdate='RESTRICT', ondelete='CASCADE'), nullable=False)
    genre = db.Column(db.Integer, db.ForeignKey('genre.genre_id',
                                                onupdate='RESTRICT', ondelete='CASCADE'))
    artiesten = db.Column(db.Text)

    def __init__(self, klant=None, genre=None, artiesten=None):
        self.klant = klant
        self.genre = genre
        self.artiesten = artiesten

    def __repr__(self):
        return '<Abonees %s %s>' % (self.mailinglist_id, self.klant)


class Artiest(db.Model):
    artiest_id = db.Column(db.Integer, primary_key=True)
    naam = db.Column(db.String(80))
    omschrijving = db.Column(db.Text, nullable=False)
    foto = db.Column(db.Text, nullable=False)
    geluids_fragment = db.Column(db.Text)
    video_fragment = db.Column(db.Text)
    website = db.Column(db.Text)
    #genre = db.Column(db.Integer, db.ForeignKey('genre.genre_id',
    #                                            onupdate='RESTRICT', ondelete='CASCADE'), nullable=False)
    genres = db.relationship('GenreArtiest', lazy='dynamic')

    def __init__(self, naam=None, omschrijving=None, foto=None, geluids_fragment=None,
                 video_fragment=None, website=None, genre=None):
        self.naam = naam
        self.omschrijving = omschrijving
        self.foto = foto
        self.geluids_fragment = geluids_fragment
        self.video_fragment = video_fragment
        self.website = website
        self.genre = genre

    def __repr__(self):
        return '<Artiest %s %s>' % (self.artiest_id, self.naam)


class GenreArtiest(db.Model):
    genre = db.Column(db.Integer, db.ForeignKey('genre.genre_id',
                                                   onupdate='RESTRICT', ondelete='CASCADE'), primary_key=True)
    artiest = db.Column(db.Integer, db.ForeignKey('artiest.artiest_id',
                                                  onupdate='RESTRICT', ondelete='CASCADE'), primary_key=True)


class Optreden(db.Model):
    optreden_id = db.Column(db.Integer, primary_key=True)
    datum = db.Column(db.Date, nullable=False)
    artiest = db.Column(db.Integer, db.ForeignKey('artiest.artiest_id',
                                                  onupdate='RESTRICT', ondelete='CASCADE'), nullable=False)
    zaal = db.Column(db.Integer, db.ForeignKey('zaal.zaal_id',
                                               onupdate='RESTRICT', ondelete='CASCADE'), nullable=False)
    uitverkocht = db.Column(db.Boolean, default=False)
    kaarten_beschikbaar = db.Column(db.Integer, nullable=False)
    kaarten_verkocht = db.Column(db.Integer, default=0)
    afgelast = db.Column(db.Boolean, nullable=False, default=False)
    datum_uitverkocht = db.Column(db.Date, nullable=True)

    def __init__(self, datum=None, artiest=None, zaal=None, kaarten_beschikbaar=None):
        self.datum = datum
        self.artiest = artiest
        self.zaal = zaal
        self.kaarten_beschikbaar = kaarten_beschikbaar

    def __repr__(self):
        return '<optrede %s %s>' % (self.optreden_id, self.datum)


class Bestelling(db.Model):
    bestelling_id = db.Column(db.Integer, primary_key=True)
    klant = db.Column(db.Integer, db.ForeignKey('klant.klant_id',
                                                onupdate="RESTRICT", ondelete="CASCADE"), nullable=False)
    optreden = db.Column(db.Integer, db.ForeignKey('optreden.optreden_id',
                                                   onupdate="RESTRICT", ondelete="CASCADE"), nullable=False)
    aantal = db.Column(db.Integer, nullable=False)

    def __init__(self, klant, optreden, aantal):
        self.klant = klant
        self.optreden = optreden
        self.aantal = aantal



db.create_all()